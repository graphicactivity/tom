<?php

return array(
    '*' => array(
        'siteUrl' => null,
        'defaultWeekStartDay' => 0,
        'enableCsrfProtection' => true,
        'omitScriptNameInUrls' => true,
        'cpTrigger' => 'admin',
        'securityKey' => getenv('SECURITY_KEY'),
    ),

    'thetom.local' => array(
        'devMode' => true,
        'environmentVariables' => array(
            'basePath' => '/Users/GraphicActivity/Sites/thetom3.local/web/',
            'baseUrl'  => 'http://thetom3.local/',
        )
    ),

    'thetom.co.nz' => array(
        'devMode' => false,
        'environmentVariables' => array(
            'basePath' => '/var/www/web/',
            'baseUrl'  => 'https://thetom.co.nz/',
        )
    )
);
